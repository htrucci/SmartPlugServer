package com.htrucci.smartplug;


import java.io.Serializable;

public class Object implements Serializable{
	private static final long serialVersionUID = 1L;
	private String Device1_name = "init1";
	private String Device2_name = "init2";
	private String Device3_name = "init3";
	private float Device1_status;
	private float Device2_status;
	private float Device3_status;
	
	public String getDevice1_name() {
		return Device1_name;
	}

	public void setDevice1_name(String device1_name) {
		Device1_name = device1_name;
	}

	public String getDevice2_name() {
		return Device2_name;
	}

	public void setDevice2_name(String device2_name) {
		Device2_name = device2_name;
	}

	public String getDevice3_name() {
		return Device3_name;
	}

	public void setDevice3_name(String device3_name) {
		Device3_name = device3_name;
	}

	public float getDevice1_status() {
		return Device1_status;
	}

	public void setDevice1_status(float device1_status) {
		Device1_status = device1_status;
	}

	public float getDevice2_status() {
		return Device2_status;
	}

	public void setDevice2_status(float device2_status) {
		Device2_status = device2_status;
	}

	public float getDevice3_status() {
		return Device3_status;
	}

	public void setDevice3_status(float device3_status) {
		Device3_status = device3_status;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public void print(){
	}
}
