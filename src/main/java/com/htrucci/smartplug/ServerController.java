package com.htrucci.smartplug;


import java.io.IOException;
import java.util.Locale;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


/**
 * Handles requests for the application home page.
 */
@Controller
public class ServerController{
	
	private static final Logger logger = LoggerFactory.getLogger(ServerController.class);
	
	@Inject
	SmartPlugServer thread;
	
	/**
	 * Simply selects the home view to render by returning its name.
	 */
	@RequestMapping(value = "/Server", method = RequestMethod.GET)
	public String home(Locale locale, Model model) {
		
		model.addAttribute("Name1", thread.ob.getDevice1_name());
		model.addAttribute("Name2", thread.ob.getDevice2_name());
		model.addAttribute("Name3", thread.ob.getDevice3_name());
		model.addAttribute("Num1", thread.ob.getDevice1_status());
		model.addAttribute("Num2", thread.ob.getDevice2_status());
		model.addAttribute("Num3", thread.ob.getDevice3_status());
		return "server";
	}
	@RequestMapping(value = "/Change", method = RequestMethod.GET)
	public void home2(Locale locale, Model model, HttpServletRequest request, HttpServletResponse response) throws IOException {
		
		model.addAttribute("Name1", thread.ob.getDevice1_name());
		model.addAttribute("Name2", thread.ob.getDevice2_name());
		model.addAttribute("Name3", thread.ob.getDevice3_name());
		String a = request.getParameter("Num1");
		String b = request.getParameter("Num2");
		String c = request.getParameter("Num3");

		thread.ob.setDevice1_status(Float.parseFloat(a));
		thread.ob.setDevice2_status(Float.parseFloat(b));
		thread.ob.setDevice3_status(Float.parseFloat(c));
		
		model.addAttribute("Num1", thread.ob.getDevice1_status());
		model.addAttribute("Num2", thread.ob.getDevice2_status());
		model.addAttribute("Num3", thread.ob.getDevice3_status());
		
		/*
		model.addAttribute("serverTime", formattedDate );*/
		response.sendRedirect("Server.do");
/*		return "server";*/
	}	
}
